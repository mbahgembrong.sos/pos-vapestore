<?php

namespace Database\Seeders;

use App\Models\Role;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pemilik = Role::where('name', 'Pemilik')->first();
        $karyawan = Role::where('name', 'Karyawan')->first();
        $user = new \App\Models\User();
        $user->name = 'Pemilik';
        $user->role_id = $pemilik->id;
        $user->email = 'pemilik@gmail.com';
        $user->password = bcrypt('pemilik');
        $user->address = 'Jl. Raya Cikarang';
        $user->phone = '081234567890';
        $user->save();
        $user = new \App\Models\User();
        $user->name = 'Karyawan';
        $user->role_id = $karyawan->id;
        $user->email = 'karyawan@gmail.com';
        $user->password = bcrypt('karyawan');
        $user->address = 'Jl. Raya Cikarang';
        $user->phone = '081234567891';
        $user->save();
    }
}
