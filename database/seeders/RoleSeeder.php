<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $pemilik = new \App\Models\Role();
        $pemilik->name = 'Pemilik';
        $pemilik->save();
        $karyawan = new \App\Models\Role();
        $karyawan->name = 'Karyawan';
        $karyawan->save();
    }
}
