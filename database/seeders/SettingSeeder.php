<?php

namespace Database\Seeders;

use App\Models\Setting;
use Illuminate\Database\Seeder;

class SettingSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $setting = new Setting();
        $setting->name = 'Toko Kita';
        $setting->alamat = 'Jl. Raya Cikarang No.1';
        $setting->phone = '081234567890';
        $setting->save();

    }
}
