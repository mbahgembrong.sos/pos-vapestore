<?php

namespace App\Repositories;

use App\Models\DetailTransaksi;
use App\Repositories\BaseRepository;

/**
 * Class DetailTransaksiRepository
 * @package App\Repositories
 * @version July 16, 2022, 10:13 am UTC
*/

class DetailTransaksiRepository extends BaseRepository
{
    /**
     * @var array
     */
    protected $fieldSearchable = [
        
    ];

    /**
     * Return searchable fields
     *
     * @return array
     */
    public function getFieldsSearchable()
    {
        return $this->fieldSearchable;
    }

    /**
     * Configure the Model
     **/
    public function model()
    {
        return DetailTransaksi::class;
    }
}
