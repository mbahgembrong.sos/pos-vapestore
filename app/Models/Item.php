<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;

/**
 * Class Item
 * @package App\Models
 * @version July 16, 2022, 10:12 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $detailTransaksis
 * @property \App\Models\Supplier $supplier
 * @property \App\Models\Category $category
 * @property char(36) $category_id
 * @property char(36) $supplier_id
 * @property integer $stock
 * @property integer $price
 * @property string $image
 */
class Item extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'items';
    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $dates = ['deleted_at'];



    public $fillable = [
        'category_id',
        'supplier_id',
        'stock',
        'sell_price',
        'image',
        'name'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'stock' => 'integer',
        'sell_price' => 'integer',
        'image' => 'string',
        'name' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'category_id' => 'required',
        'supplier_id' => 'required',
        'sell_price' => 'required|integer',
        'image' => 'nullable|file|mimes:jpeg,png,jpg,gif,svg|max:2048',
        'name' => 'required|string'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function detailTransaksis()
    {
        return $this->hasMany(\App\Models\DetailTransaksi::class, 'item_id');
    }
    public function detailStockTransaksi()
    {
        return $this->hasMany(\App\Models\DetailStockItem::class, 'item_id')->orderBy('created_at', 'desc');
    }
    public function lastBuyPrice()
    {
        $detailStockItem = DetailStockItem::where('item_id', $this->id)->orderBy('created_at', 'desc')->get();
        if ($detailStockItem->count() > 0) {
            $oldPrice = $detailStockItem[1]->buy_price ?? 0;
            $newPrice = ($detailStockItem[0]->buy_price ?? 0);
        } else {
            $oldPrice = 0;
            $newPrice = 0;
        }
        return [
            'new_buy_price' => $newPrice,
            'last_buy_date' => $oldPrice,
            'status' => $newPrice > $oldPrice ? 'up' : 'down',
            'margin' => $newPrice > $oldPrice ? ($newPrice - $oldPrice) / ($newPrice == 0 ? 1 : $newPrice) * 100 : ($oldPrice - $newPrice) / ($oldPrice == 0 ? 1 : $oldPrice) * 100
        ];
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function supplier()
    {
        return $this->belongsTo(\App\Models\Supplier::class, 'supplier_id', 'id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function category()
    {
        return $this->belongsTo(\App\Models\Category::class, 'category_id', 'id')->withTrashed();
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
