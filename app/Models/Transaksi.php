<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;

/**
 * Class Transaksi
 * @package App\Models
 * @version July 16, 2022, 10:12 am UTC
 *
 * @property \App\Models\User $user
 * @property \App\Models\Member $member
 * @property \Illuminate\Database\Eloquent\Collection $detailTransaksis
 * @property char(36) $user_id
 * @property char(36) $member_id
 * @property integer $grand_total
 */
class Transaksi extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'transaksis';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $dates = ['deleted_at'];



    public $fillable = [
        'user_id',
        'member_id',
        'name',
        'telp',
        'address',
        'grand_total',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'grand_total' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'member_id' => 'nullable',
        'grand_total' => 'required|integer'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function user()
    {
        return $this->belongsTo(\App\Models\User::class, 'user_id', 'id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function member()
    {
        return $this->belongsTo(\App\Models\Member::class, 'member_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function detailTransaksi()
    {
        return $this->hasMany(\App\Models\DetailTransaksi::class, 'transaksi_id');
    }
    public function getCreatedAtAttribute($date)
    {
        return \Carbon\Carbon::parse($date)->format('d-m-Y');
    }

    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}