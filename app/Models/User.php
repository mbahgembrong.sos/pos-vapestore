<?php

namespace App\Models;

// use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Ramsey\Uuid\Uuid as Generator;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

/**
 * Class User
 * @package App\Models
 * @version July 16, 2022, 10:09 am UTC
 *
 * @property \App\Models\Role $role
 * @property \Illuminate\Database\Eloquent\Collection $transaksis
 * @property char(36) $role_id
 * @property string $name
 * @property string $email
 * @property string|\Carbon\Carbon $email_verified_at
 * @property string $password
 * @property string $address
 * @property string $phone
 * @property string $remember_token
 */
class User extends Authenticatable
{

    use SoftDeletes;
    use Notifiable;

    use HasFactory;

    public $table = 'users';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $dates = ['deleted_at'];
    protected $hidden = ['password'];

    public $fillable = [
        'role_id',
        'name',
        'email',
        'email_verified_at',
        'password',
        'address',
        'phone',
        'remember_token'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'email' => 'string',
        'address' => 'string',
        'phone' => 'string',

    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'role_id' => 'required',
        'name' => 'required|string|max:255|string|max:255',
        'email' => 'required|string|max:255|email|unique:users,email',
        'password' => 'required|string|max:255|string|max:255',
        'address' => 'nullable|string|nullable',
        'phone' => 'required|string:13|numeric|unique:users,phone',
        'created_at' => 'nullable|nullable'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function role()
    {
        return $this->belongsTo(\App\Models\Role::class, 'role_id','id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function transaksis()
    {
        return $this->hasMany(\App\Models\Transaksi::class, 'user_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
