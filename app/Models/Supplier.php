<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class Supplier
 * @package App\Models
 * @version July 16, 2022, 10:10 am UTC
 *
 * @property \Illuminate\Database\Eloquent\Collection $items
 * @property string $name
 * @property string $sales
 * @property string $address
 * @property string $phone
 */
class Supplier extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'suppliers';
    protected $primaryKey = 'id';
    public $incrementing = false;

    protected $dates = ['deleted_at'];



    public $fillable = [
        'name',
        'sales',
        'address',
        'phone'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'name' => 'string',
        'sales' => 'string',
        'address' => 'string',
        'phone' => 'string'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [
        'name' => 'required|string',
        'sales' => 'required|string',
        'address' => 'required|string',
        'phone' => 'required|string:13|numeric|unique:suppliers,phone'
    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     **/
    public function items()
    {
        return $this->hasMany(\App\Models\Item::class, 'item_id');
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
