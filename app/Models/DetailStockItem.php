<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Ramsey\Uuid\Uuid as Generator;

class DetailStockItem extends Model
{
    use SoftDeletes;
    use HasFactory;
    public $table = 'detail_stock_items';
    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $dates = ['deleted_at'];



    public $fillable = [
        'item_id',
        'new_stock',
        'buy_price',
    ];
    public function item()
    {
        return $this->belongsTo(Item::class, 'item_id')->withTrashed();
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}