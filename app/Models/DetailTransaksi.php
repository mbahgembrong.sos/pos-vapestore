<?php

namespace App\Models;

use Eloquent as Model;
use Illuminate\Database\Eloquent\SoftDeletes;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Ramsey\Uuid\Uuid as Generator;
/**
 * Class DetailTransaksi
 * @package App\Models
 * @version July 16, 2022, 10:13 am UTC
 *
 * @property \App\Models\Transaksi $transaksi
 * @property \App\Models\Item $item
 * @property char(36) $transaksi_id
 * @property char(36) $item_id
 * @property integer $total
 * @property integer $price
 */
class DetailTransaksi extends Model
{
    use SoftDeletes;

    use HasFactory;

    public $table = 'detail_transaksis';
    protected $primaryKey = 'id';
    public $incrementing = false;


    protected $dates = ['deleted_at'];



    public $fillable = [
        'transaksi_id',
        'item_id',
        'total',
        'price',
        'created_at',
        'updated_at'
    ];

    /**
     * The attributes that should be casted to native types.
     *
     * @var array
     */
    protected $casts = [
        'total' => 'integer',
        'price' => 'integer'
    ];

    /**
     * Validation rules
     *
     * @var array
     */
    public static $rules = [

    ];

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function transaksi()
    {
        return $this->belongsTo(\App\Models\Transaksi::class, 'transaksi_id')->withTrashed();
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     **/
    public function item()
    {
        return $this->belongsTo(\App\Models\Item::class, 'item_id')->withTrashed();
    }
    protected static function boot()
    {
        parent::boot();

        static::creating(function ($model) {
            try {
                $model->id = Generator::uuid4()->toString();
            } catch (UnableToBuildUuidException $e) {
                abort(500, $e->getMessage());
            }
        });
    }
}
