<?php

namespace App\DataTables;

use App\Models\Transaksi;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class TransaksiDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query->where('user_id', 'LIKE', '%' . (auth()->user()->role->name != 'Pemilik' ? auth()->user()->id : '') . '%')->with(['member', 'detailTransaksi', 'user']));

        return $dataTable->addColumn('action', 'transaksis.datatables_actions')->addIndexColumn()
            ->editColumn('ref', function ($transaksi) {
                return explode('-', $transaksi->id)[0];
            })->editColumn('member_id', function ($transaksi) {
            return $transaksi->name ?? $transaksi->member()->first()->name ?: 'non member';
        })->editColumn('count_item', function ($transaksi) {
            if (auth()->user()->role->name == 'Pemilik') {
                return '<span >' . $transaksi->detailTransaksi->count() . ' items</span>';
            } else {
                // return
                $data = [];
                foreach ($transaksi->detailTransaksi as $item) {
                    array_push($data, '<tr>
                <td>' . $item->total . ' x ' . $item->item->name . '</td>
                <td></td>
                <td class="text-right">' . $item->price . '</td>
            </tr>');
                }
                return implode($data);
            }
        })
            ->rawColumns(['member_id', 'action', 'count_item']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Transaksi $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Transaksi $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addColumnBefore([
                'defaultContent' => '',
                'data' => 'DT_RowIndex',
                'name' => 'DT_RowIndex',
                'title' => 'No',
                'searchable' => false,
                'orderable' => false,
                'exportable' => false,
                'printable' => false,
                'footer' => '',
            ])
            ->minifiedAjax()
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'ref' => new Column(['title' => 'ref', 'data' => 'ref', 'searchable' => true]),
            'member_id' => new Column(['title' => __('models/transaksis.fields.member_id'), 'data' => 'member_id', 'searchable' => true]),
            'count_item' => new Column(['title' => __('models/transaksis.fields.count_item'), 'data' => 'count_item', 'searchable' => true]),
            'grand_total' => new Column(['title' => __('models/transaksis.fields.grand_total'), 'data' => 'grand_total']),
            'created_at' => new Column(['title' => 'Tanggal Pesan', 'data' => 'created_at']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'transaksis_datatable_' . time();
    }
}