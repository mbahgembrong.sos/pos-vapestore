<?php

namespace App\DataTables;

use App\Models\Item;
use Illuminate\Support\Facades\Storage;
use Yajra\DataTables\Services\DataTable;
use Yajra\DataTables\EloquentDataTable;
use Yajra\DataTables\Html\Column;

class ItemDataTable extends DataTable
{
    /**
     * Build DataTable class.
     *
     * @param mixed $query Results from query() method.
     * @return \Yajra\DataTables\DataTableAbstract
     */
    public function dataTable($query)
    {
        $dataTable = new EloquentDataTable($query);

        return $dataTable->addColumn('action', 'items.datatables_actions')->addIndexColumn()->editColumn('image', function ($items) {
            return '<img class="circular--square--index" src="' . ($items->image != null ? Storage::url('items/' . $items->image) : "") . '"">';
        })
            ->editColumn('category_id', function ($items) {
                return $items->category()->first()->name;
            })
            ->editColumn('supplier_id', function ($items) {
                return $items->supplier()->first()->name;
            })
            ->editColumn('buy_price', function ($items) {
                return '<span >' . $items->lastBuyPrice()['new_buy_price'] . ' '
                    // . ($items->lastBuyPrice()['status'] == "up" ? '<i class="fas fa-arrow-up text-danger"> ' . (int)$items->lastBuyPrice()["margin"] . '%</i>' : '<i class="fa fa-arrow-down text-success">' . (int)$items->lastBuyPrice()["margin"] . '%</i>')
                    . '</span>';
            })
            ->rawColumns(['image', 'action', 'buy_price', 'supplier_id', 'category_id']);
    }

    /**
     * Get query source of dataTable.
     *
     * @param \App\Models\Item $model
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function query(Item $model)
    {
        return $model->newQuery();
    }

    /**
     * Optional method if you want to use html builder.
     *
     * @return \Yajra\DataTables\Html\Builder
     */
    public function html()
    {
        return $this->builder()
            ->columns($this->getColumns())
            ->addColumnBefore([
                'defaultContent' => '',
                'data' => 'DT_RowIndex',
                'name' => 'DT_RowIndex',
                'title' => 'No',
                'searchable' => false,
                'orderable' => false,
                'exportable' => false,
                'printable' => false,
                'footer' => '',
            ])
            ->minifiedAjax()
            ->addColumn([
                'defaultContent' => '',
                'data' => 'buy_price',
                'name' => 'buy_price',
                'title' => __('models/items.fields.buy_price'),
                'searchable' => false,
                'orderable' => false,
                'exportable' => false,
                'printable' => false,
                'footer' => '',
            ])
            ->addAction(['width' => '120px', 'printable' => false, 'title' => __('crud.action')])
            ->parameters([
                'dom' => 'Bfrtip',
                'stateSave' => true,
                'order' => [[0, 'desc']],
                'buttons' => [
                    [
                        'extend' => 'create',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-plus"></i> ' . __('auth.app.create') . ''
                    ],
                    [
                        'extend' => 'export',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-download"></i> ' . __('auth.app.export') . ''
                    ],
                    [
                        'extend' => 'print',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-print"></i> ' . __('auth.app.print') . ''
                    ],
                    [
                        'extend' => 'reset',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-undo"></i> ' . __('auth.app.reset') . ''
                    ],
                    [
                        'extend' => 'reload',
                        'className' => 'btn btn-default btn-sm no-corner',
                        'text' => '<i class="fa fa-refresh"></i> ' . __('auth.app.reload') . ''
                    ],
                ],

            ]);
    }

    /**
     * Get columns.
     *
     * @return array
     */
    protected function getColumns()
    {
        return [
            'name' => new Column(['title' => __('models/items.fields.name'), 'data' => 'name']),
            'category_id' => new Column(['title' => __('models/items.fields.category_id'), 'data' => 'category_id']),
            'supplier_id' => new Column(['title' => __('models/items.fields.supplier_id'), 'data' => 'supplier_id']),
            'stock' => new Column(['title' => __('models/items.fields.stock'), 'data' => 'stock']),
            'sell_price' => new Column(['title' => __('models/items.fields.sell_price'), 'data' => 'sell_price']),
            // 'buy_price' => new Column(['title' => __('models/items.fields.buy_price'), 'data' => 'buy_price']),
            'image' => new Column(['title' => __('models/items.fields.image'), 'data' => 'image']),
        ];
    }

    /**
     * Get filename for export.
     *
     * @return string
     */
    protected function filename()
    {
        return 'items_datatable_' . time();
    }
}