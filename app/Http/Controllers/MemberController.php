<?php

namespace App\Http\Controllers;

use App\DataTables\MemberDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateMemberRequest;
use App\Http\Requests\UpdateMemberRequest;
use App\Repositories\MemberRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Alert;

class MemberController extends AppBaseController
{
    /** @var  MemberRepository */
    private $memberRepository;

    public function __construct(MemberRepository $memberRepo)
    {
        $this->memberRepository = $memberRepo;
    }

    /**
     * Display a listing of the Member.
     *
     * @param MemberDataTable $memberDataTable
     * @return Response
     */
    public function index(MemberDataTable $memberDataTable)
    {
        return $memberDataTable->render('members.index');
    }

    /**
     * Show the form for creating a new Member.
     *
     * @return Response
     */
    public function create()
    {
        return view('members.create');
    }

    /**
     * Store a newly created Member in storage.
     *
     * @param CreateMemberRequest $request
     *
     * @return Response
     */
    public function store(CreateMemberRequest $request)
    {
        $input = $request->all();

        $member = $this->memberRepository->create($input);

        Alert::success(__('messages.success'), 'Data ' . __('models/members.singular') . ' ' . __('messages.saved'));

        return redirect(route('members.index'));
    }

    /**
     * Display the specified Member.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $member = $this->memberRepository->find($id);

        if (empty($member)) {
            Flash::error(__('messages.not_found', ['model' => __('models/members.singular')]));
            Alert::error(__('messages.error'), 'Data ' . __('models/members.singular') . ' ' . __('messages.not_found'));

            return redirect(route('members.index'));
        }

        return view('members.show')->with('member', $member);
    }

    /**
     * Show the form for editing the specified Member.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $member = $this->memberRepository->find($id);

        if (empty($member)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/members.singular') . ' ' . __('messages.not_found'));

            return redirect(route('members.index'));
        }

        return view('members.edit')->with('member', $member);
    }

    /**
     * Update the specified Member in storage.
     *
     * @param  int              $id
     * @param UpdateMemberRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateMemberRequest $request)
    {
        $member = $this->memberRepository->find($id);

        if (empty($member)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/members.singular') . ' ' . __('messages.not_found'));

            return redirect(route('members.index'));
        }

        $member = $this->memberRepository->update($request->all(), $id);

        Alert::success(__('messages.success'), 'Data ' . __('models/members.singular') . ' ' . __('messages.updated'));
        return redirect(route('members.index'));
    }

    /**
     * Remove the specified Member from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $member = $this->memberRepository->find($id);

        if (empty($member)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/members.singular') . ' ' . __('messages.not_found'));

            return redirect(route('members.index'));
        }

        $this->memberRepository->delete($id);

        Alert::success(__('messages.success'), 'Data ' . __('models/members.singular') . ' ' . __('messages.deleted'));

        return redirect(route('members.index'));
    }
}
