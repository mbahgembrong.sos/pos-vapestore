<?php

namespace App\Http\Controllers;

use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;
use RealRashid\SweetAlert\Facades\Alert;

class SettingController extends Controller
{

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function edit(Setting $setting)
    {
        $setting = Setting::all()->first();
        return view('setting.index', compact('setting'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateSettingRequest  $request
     * @param  \App\Models\Setting  $setting
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $setting = Setting::all()->first();
        $setting->update($request->all());
        if ($request->hasFile('logo')) {
            move_uploaded_file($request->file('logo')->path(), public_path('img/' . 'logo.png'));
        }
        $setting->save();
        Alert::success('Berhasil', 'Data berhasil diubah');
        return redirect()->back();
    }

}
