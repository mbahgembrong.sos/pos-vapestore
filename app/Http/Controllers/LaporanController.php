<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksi;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;

class LaporanController extends Controller
{
    public function index(Request $request)
    {
        $laporan = [];
        $labels = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        for ($i = 1; $i <= 12; $i++) {
            $sumTransaksiMonths = Transaksi::whereYear('created_at', date('Y'))->whereMonth('created_at', $i)->sum('grand_total');
            $itemsBuyPrice = 0;
            $detailTransaksiMonths = DetailTransaksi::WhereMonth('created_at', $i)->get();
            foreach ($detailTransaksiMonths as $detailTransaksi => $value) {
                $itemsBuyPrice += $value->item->detailStockTransaksi()->orderBy('created_at', 'desc')->first()->buy_price * $value->total;
            }
            $pendapatanMonths = $sumTransaksiMonths - $itemsBuyPrice;

            array_push($laporan, [
                "date" => $labels[$i - 1],
                "pendapatan" => $pendapatanMonths,
                "transaksi" => $sumTransaksiMonths,
                "items_buy_price" => $itemsBuyPrice,
                "items" => $detailTransaksiMonths->groupBy('item_id')->map(function ($item) {
                    return [
                        'name' => $item->first()->item->name,
                        'total' => $item->sum('total'),
                    ];
                })->values(),
            ]);
        }
        // sum pendapatan
        $laporan = collect($laporan);
        return view('laporan.index', compact('laporan'));
    }
}