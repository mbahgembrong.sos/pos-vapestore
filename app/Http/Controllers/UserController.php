<?php

namespace App\Http\Controllers;

use App\DataTables\UserDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Repositories\UserRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Alert;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use RealRashid\SweetAlert\Facades\Alert as FacadesAlert;

class UserController extends AppBaseController
{
    /** @var  UserRepository */
    private $userRepository;

    public function __construct(UserRepository $userRepo)
    {
        $this->userRepository = $userRepo;
    }

    /**
     * Display a listing of the User.
     *
     * @param UserDataTable $userDataTable
     * @return Response
     */
    public function index(UserDataTable $userDataTable)
    {
        return $userDataTable->render('users.index');
    }

    /**
     * Show the form for creating a new User.
     *
     * @return Response
     */
    public function create()
    {
        $roles = \App\Models\Role::pluck('name', 'id');
        return view('users.create', compact('roles'));
    }

    /**
     * Store a newly created User in storage.
     *
     * @param CreateUserRequest $request
     *
     * @return Response
     */
    public function store(CreateUserRequest $request)
    {
        $input = $request->all();
        $input['password'] = bcrypt($input['password']);

        $user = $this->userRepository->create($input);
        Alert::success(__('messages.success'), 'Data ' . __('models/users.singular') . ' ' . __('messages.saved'));
        return redirect(route('users.index'));
    }

    /**
     * Display the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));

            return redirect(route('users.index'));
        }

        return view('users.show', compact('user'));
    }

    /**
     * Show the form for editing the specified User.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));

            return redirect(route('users.index'));
        }
        $roles = \App\Models\Role::pluck('name', 'id');

        return view('users.edit', compact('user', 'roles'));
    }
    /**
     * Update the specified User in storage.
     *
     * @param  int              $id
     * @param UpdateUserRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateUserRequest $request)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));

            return redirect(route('users.index'));
        }
        if ($request->has('password')) {
            $request->merge(['password' => bcrypt($request->get('password'))]);
        }

        $user = $this->userRepository->update($request->all(), $id);
        Alert::success(__('messages.success'), 'Data ' . __('models/users.singular') . ' ' . __('messages.updated'));

        return redirect(route('users.index'));
    }
    public function changeProfile(Request $request,$id)
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            return response()->json(['error' => 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found')], 404);
        }
        $user->name = $request->name;
        $user->email = $request->email;
        $user->save();
        return back();
    }
    public function changePassword(Request $request,$id)
    {
        $user = $this->userRepository->find($id);
        if (empty($user)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));
        }
        if (Hash::check($request->password_current, $user->password)) {
            $user->password = bcrypt($request->password);
            $user->save();
            Alert::success(__('messages.success'), 'Data ' . __('models/users.singular') . ' ' . __('messages.updated'));
            return back();
        }else{
            Alert::error(__('messages.error'), 'Password lama salah');
            return back();
        }
    }


    /**
     * Remove the specified User from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $user = $this->userRepository->find($id);

        if (empty($user)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));
            return redirect(route('users.index'));
        }

        $this->userRepository->delete($id);

        Alert::success(__('messages.success'), 'Data ' . __('models/users.singular') . ' ' . __('messages.deleted'));

        return redirect(route('users.index'));
    }
}
