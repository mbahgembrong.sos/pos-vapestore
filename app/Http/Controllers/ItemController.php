<?php

namespace App\Http\Controllers;

use App\DataTables\ItemDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateItemRequest;
use App\Http\Requests\UpdateItemRequest;
use App\Repositories\ItemRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Alert;
use App\Models\Category;
use App\Models\DetailStockItem;
use App\Models\Item;
use App\Models\Supplier;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class ItemController extends AppBaseController
{
    /** @var  ItemRepository */
    private $itemRepository;

    public function __construct(ItemRepository $itemRepo)
    {
        $this->itemRepository = $itemRepo;
    }

    /**
     * Display a listing of the Item.
     *
     *
     */
    public function index(ItemDataTable $itemDataTable)
    {
        // dd(Item::with('category', 'supplier', 'lastBuyPrice')->get());
        return $itemDataTable->render('items.index');
    }
    public function jsonShow($id)
    {
        $items = Item::where('id', $id)->with('category')->first();
        return response()->json($items);
    }

    // {
    //     $items = $this->itemRepository->all();
    //     return view('items.index')->with('items', $items);
    // }

    /**
     * Show the form for creating a new Item.
     *
     * @return Response
     */
    public function create()
    {
        $categorys = Category::pluck("name", "id");
        $suppliers = Supplier::pluck("name", "id");
        return view('items.create', compact(['categorys', 'suppliers']));
    }

    /**
     * Store a newly created Item in storage.
     *
     * @param CreateItemRequest $request
     *
     * @return Response
     */
    public function store(CreateItemRequest $request)
    {
        $input = $request->all();
        if ($request->hasFile('image')) {
            $imageName = time() . $request->file('image')->getClientOriginalName();
            Storage::disk('public')->put('items/' .  $imageName, file_get_contents($request->file('image')->getRealPath()));
            $input['image'] = $imageName;
        }
        $input['stock'] = 0;
        $item = $this->itemRepository->create($input);

        Alert::success(__('messages.success'), 'Data ' . __('models/items.singular') . ' ' . __('messages.saved'));

        return redirect(route('items.index'));
    }

    /**
     * Display the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/items.singular') . ' ' . __('messages.not_found'));

            return redirect(route('items.index'));
        }

        return view('items.show')->with('item', $item);
    }

    /**
     * Show the form for editing the specified Item.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/items.singular') . ' ' . __('messages.not_found'));

            return redirect(route('items.index'));
        }
        $categorys = Category::pluck("name", "id");
        $suppliers = Supplier::pluck("name", "id");
        return view('items.edit', compact(['categorys', 'suppliers']))->with('item', $item);
    }

    /**
     * Update the specified Item in storage.
     *
     * @param  int              $id
     * @param UpdateItemRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateItemRequest $request)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/items.singular') . ' ' . __('messages.not_found'));

            return redirect(route('items.index'));
        }
        $input= $request->all();
      
        if ($request->hasFile('image')) {
            $imageName = time() . $request->file('image')->getClientOriginalName();
            Storage::disk('public')->put('items/' .  $imageName, file_get_contents($request->file('image')->getRealPath()));
            $input['image']= $imageName;
        }

        $item = $this->itemRepository->update($input, $id);

        Alert::success(__('messages.success'), 'Data ' . __('models/items.singular') . ' ' . __('messages.updated'));

        return redirect(route('items.index'));
    }

    public function stock(Request $request)
    {
        // dd($request);
        $this->validate($request, [
            'id' => 'required',
            'new_stock' => 'required|integer',
            'buy_price' => 'required|integer',
        ]);

        DetailStockItem::create([
            'item_id' => $request->id,
            'new_stock' => $request->new_stock,
            'buy_price' => $request->buy_price,
        ]);
        $item = Item::find($request->id);
        $item->stock = $item->stock + $request->new_stock;
        $item->save();
        Alert::success(__('messages.success'), 'Data ' . __('models/items.singular') . ' ' . __('messages.updated'));
        return redirect(route('items.index'));
    }

    /**
     * Remove the specified Item from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $item = $this->itemRepository->find($id);

        if (empty($item)) {

            Alert::error(__('messages.error'), 'Data ' . __('models/items.singular') . ' ' . __('messages.not_found'));

            return redirect(route('items.index'));
        }

        $this->itemRepository->delete($id);

        Alert::success(__('messages.success'), 'Data ' . __('models/items.singular') . ' ' . __('messages.deleted'));

        return redirect(route('items.index'));
    }
}
