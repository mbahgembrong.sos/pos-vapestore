<?php

namespace App\Http\Controllers;

use App\Models\DetailTransaksi;
use App\Models\Item;
use App\Models\Member;
use App\Models\Transaksi;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {

        $sumTransaksiDays = Transaksi::whereDate('created_at', Carbon::today())->sum('grand_total');
        $itemsBuyPrice = 0;
        $detailTransaksiDays = DetailTransaksi::WhereDate('created_at', Carbon::today())->get();
        foreach ($detailTransaksiDays as $detailTransaksi => $value) {
            $itemsBuyPrice += $value->item->detailStockTransaksi[0]->buy_price * $value->total;
        }
        $pendapatanDays = $sumTransaksiDays - $itemsBuyPrice;
        $countTransaksiDays = Transaksi::whereDate('created_at', Carbon::today())->count();
        $countItem = Item::all()->count();

        $days = [
            "Sunday",
            "Monday",
            "Tuesday",
            "Wednesday",
            "Thursday",
            "Friday",
            "Saturday"
        ];
        $items = Item::all();
        $itemPopulerDay = [];
        $itemPopulerMonth = [];
        foreach ($items as $item) {
            $itemDay = [];
            for ($i = Carbon::now()->startOfWeek(); $i <= Carbon::now()->endOfWeek(); $i->modify('+1 day')) {
                $perDay = DetailTransaksi::WhereDate('created_at', $i->format('Y-m-d'))
                    ->where('item_id', $item->id)->sum('total');
                array_push($itemDay, $perDay);
            }
            array_push($itemPopulerDay, ["label" => $item->name, "data" => $itemDay, 'lineTension' => 0.4,]);
        }

        $itemPopulerDay = json_encode($itemPopulerDay, JSON_PRETTY_PRINT);

        return view('home', compact('itemPopulerDay', 'days', 'pendapatanDays', 'countTransaksiDays', 'countItem'));
    }
}