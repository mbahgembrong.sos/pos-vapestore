<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\DetailStockItem;
use App\Models\DetailTransaksi;
use App\Models\Item;
use App\Models\Member;
use App\Models\Supplier;
use App\Models\Transaksi;
use App\Models\User;
use DateTime;
use Illuminate\Http\Request;
use Faker;

class TestController extends Controller
{
    public function index()
    {
        $this->barang();
        $begin = new DateTime("2022-07-01");
        $end   = new DateTime("2022-07-25");
        $roleKaryawan = User::where('name', 'Karyawan')->first()->id;
        for ($i = $begin; $i < $end; $i->modify('+1 day')) {
            for ($d = 0; $d < 5; $d++) {
                $transaksi = new Transaksi;
                $transaksi->user_id = User::where('id', $roleKaryawan)->first()->id;
                $member  = null;
                $memberTrue = rand(1, 2);
                if ($memberTrue == 1) {
                    $getMember = Member::all();
                    $member = $getMember[rand(0, $getMember->count() - 1)]->id;
                    $transaksi->member_id = $member;
                }
                $transaksi->grand_total = 0;
                $transaksi->save();
                $grandTotal = 0;
                for ($j = 0; $j < rand(1, 5); $j++) {
                    $itemAll = Item::all();
                    $item = $itemAll[rand(0, $itemAll->count() - 1)];
                    $detailTransaksi = new DetailTransaksi;
                    $detailTransaksi->transaksi_id = $transaksi->id;
                    $detailTransaksi->item_id = $item->id;
                    $detailTransaksi->total = rand(1, 9);
                    $itemUpdate = Item::find($item->id);
                    if ($member == null) {
                        $detailTransaksi->price = $itemUpdate->sell_price *  $detailTransaksi->total;
                    } else {
                        $total = ($itemUpdate->sell_price *  $detailTransaksi->total);
                        $detailTransaksi->price = $total - ($total * $item->category->discount / 100);
                    }
                    $grandTotal += $detailTransaksi->price;
                    $itemUpdate->stock = $itemUpdate->stock - $detailTransaksi->total;
                    $itemUpdate->save();
                    $detailTransaksi->created_at
                        = $i->format("Y-m-d H:i:s");
                    $detailTransaksi->updated_at
                        = $i->format("Y-m-d H:i:s");
                    $detailTransaksi->save();
                }
                $transaksi->created_at
                    = $i->format("Y-m-d H:i:s");
                $transaksi->updated_at
                    = $i->format("Y-m-d H:i:s");
                $transaksi->grand_total = $grandTotal;
                $transaksi->save();
            }
        }

        return response()->json(['message' => 'Successfully seeded database'], 200);
    }
    public function barang()
    {
        Supplier::create([
            'name' => 'Vaporesia', 'sales' => 'Ahmad Galang', 'address' => 'Jl Rancabolang, Manjahlega. Bandung', 'phone' => '081958222976'
        ]);
        Supplier::create([
            'name' => 'JuiceNation', 'sales' => 'Indra', 'address' => 'Jl Cirangrang Barat, Kota Bandung', 'phone' => '081123117189'
        ]);
        Supplier::create([
            'name' => 'JVS', 'sales' => 'Dandi', 'address' => 'Jl Ciputat Raya, Jakarta Selatan', 'phone' => '089645334867'
        ]);
        Supplier::create([
            'name' => 'Emkay', 'sales' => 'Rico', 'address' => 'Taman Tekno Boulevard, BSD City, Tangerang Selatan', 'phone' => '089322677158'
        ]);
        Supplier::create([
            'name' => 'RayVapor', 'sales' => 'Reynaldi', 'address' => 'Jl Karang Tengah Raya, DKI Jakarta', 'phone' => '087145888576'
        ]);
        Supplier::create([
            'name' => 'Bogo E-Liquid', 'sales' => 'Bagas', 'address' => 'Jl Taman Nusa Indah, Malang', 'phone' => '082666713589'
        ]);
        Supplier::create([
            'name' => 'VapeZoo', 'sales' => 'Karisma', 'address' => 'Jl Panjang, Duri Kepa, Jakarta Barat', 'phone' => '089222727878'
        ]);
        Supplier::create([
            'name' => 'VapeBoss', 'sales' => 'Nanda', 'address' => 'Jl Sumatera, Gubeng, Surabaya', 'phone' => '081949672324'
        ]);
        Supplier::create([
            'name' => 'GeekVape', 'sales' => 'Rony', 'address' => 'Jl Medan Amplas, Kota medan', 'phone' => '081467882992'
        ]);
        Supplier::create([
            'name' => 'SMOK', 'sales' => 'Alex', 'address' => 'Jl Raya Sesetan, Bali', 'phone' => '085127777534'
        ]);
        Category::create([
            'name' => 'Liquid', 'discount' => '5'
        ]);
        Category::create([
            'name' => 'Coil', 'discount' => '5'
        ]);
        Category::create([
            'name' => 'Pod', 'discount' => '10'
        ]);
        Category::create([
            'name' => 'Mod', 'discount' => '10'
        ]);
        Category::create([
            'name' => 'Cotton', 'discount' => '5'
        ]);
        Category::create([
            'name' => 'Toolkit', 'discount' => '0.5'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Liquid')->first()->id, 'supplier_id' => Supplier::where('name', 'JuiceNation')->first()->id, 'name' => 'Alacarte', 'stock' => '1000', 'sell_price' => '180000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '150000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Liquid')->first()->id, 'supplier_id' => Supplier::where('name', 'Bogo E-Liquid')->first()->id, 'name' => 'Bogo', 'stock' => '1000', 'sell_price' => '120000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '90000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Liquid')->first()->id, 'supplier_id' => Supplier::where('name', 'Emkay')->first()->id, 'name' => 'Bondan', 'stock' => '1000', 'sell_price' => '140000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '110000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Coil')->first()->id, 'supplier_id' => Supplier::where('name', 'Vaporesia')->first()->id, 'name' => 'Legalize', 'stock' => '1000', 'sell_price' => '65000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '50000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Coil')->first()->id, 'supplier_id' => Supplier::where('name', 'JVS')->first()->id, 'name' => 'Champions Coil', 'stock' => '1000', 'sell_price' => '45000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '30000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Pod')->first()->id, 'supplier_id' => Supplier::where('name', 'SMOK')->first()->id, 'name' => 'Nord', 'stock' => '1000', 'sell_price' => '235000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '200000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Mod')->first()->id, 'supplier_id' => Supplier::where('name', 'GeekVape')->first()->id, 'name' => 'Aegis', 'stock' => '1000', 'sell_price' => '400000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '350000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Mod')->first()->id, 'supplier_id' => Supplier::where('name', 'VapeZoo')->first()->id, 'name' => 'Hexomh', 'stock' => '1000', 'sell_price' => '3000000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '2700000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Cotton')->first()->id, 'supplier_id' => Supplier::where('name', 'VapeBoss')->first()->id, 'name' => 'Cotton Bacon', 'stock' => '1000', 'sell_price' => '50000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '40000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Cotton')->first()->id, 'supplier_id' => Supplier::where('name', 'RayVapor')->first()->id, 'name' => 'Kendo', 'stock' => '1000', 'sell_price' => '70000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '60000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Toolkit')->first()->id, 'supplier_id' => Supplier::where('name', 'Vaporesia')->first()->id, 'name' => 'Gear Master 4', 'stock' => '1000', 'sell_price' => '130000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '120000'
        ]);
        $item = Item::create([
            'category_id' => Category::where('name', 'Toolkit')->first()->id, 'supplier_id' => Supplier::where('name', 'Vaporesia')->first()->id, 'name' => 'Gear Master 5', 'stock' => '1000', 'sell_price' => '170000'
        ]);
        DetailStockItem::create([
            'item_id' => $item->id, 'new_stock' => '1000', 'buy_price' => '150000'
        ]);
        $faker = Faker\Factory::create();
        for ($i = 0; $i < 10; $i++) {
            Member::create([
                'name' => $faker->firstNameFemale, 'alamat' => $faker->city, 'phone' => '08' . $faker->numerify('##########')
            ]);
        }
    }
}
