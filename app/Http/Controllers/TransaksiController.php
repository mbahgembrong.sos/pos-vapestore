<?php

namespace App\Http\Controllers;

use App\DataTables\TransaksiDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateTransaksiRequest;
use App\Http\Requests\UpdateTransaksiRequest;
use App\Repositories\TransaksiRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Alert;
use App\Models\DetailTransaksi;
use App\Models\Item;
use App\Models\Member;
use App\Models\Setting;
use App\Models\Transaksi;
use App\Models\User;

class TransaksiController extends AppBaseController
{
    /** @var  TransaksiRepository */
    private $transaksiRepository;

    public function __construct(TransaksiRepository $transaksiRepo)
    {
        $this->transaksiRepository = $transaksiRepo;
    }

    /**
     * Display a listing of the Transaksi.
     *
     * @param TransaksiDataTable $transaksiDataTable
     * @return Response
     */
    public function index(TransaksiDataTable $transaksiDataTable)
    {
        $transaksis = Transaksi::orderBy('created_at', 'ASC')->get();
        $transaksis->each(function ($transaksi) {
            if ($transaksi->detailTransaksi->count() <= 0) {
                $transaksi->delete();
            }
        });
        return $transaksiDataTable->render('transaksis.index');
    }

    /**
     * Show the form for creating a new Transaksi.
     *
     * @return Response
     */
    public function create()
    {
        $members = Member::pluck('name', 'id');
        $items = Item::where('stock', '>', 0)->get();
        return view('transaksis.create', compact(['members', 'items']));
    }

    /**
     * Store a newly created Transaksi in storage.
     *
     * @param CreateTransaksiRequest $request
     *
     * @return Response
     */
    public function store(CreateTransaksiRequest $request)
    {
        try {
            $input = $request->all();
            $transaksi = new Transaksi;
            $transaksi->user_id = auth()->user()->id;
            if ($request->has('member_id') && $request->member == 'Member') {
                $transaksi->member_id = $request->member_id;
                $member = Member::find($request->member_id);
                $transaksi->name = $member->name;
                $transaksi->telp = $member->phone;
                $transaksi->address = $member->alamat;
            } else {
                $transaksi->name = $request->name;
                $transaksi->telp = $request->telp;
                $transaksi->address = $request->address;
            }
            $transaksi->grand_total = $request->grand_total;
            $transaksi->save();
            foreach ($request->items as $key => $item) {
                $detailTransaksi = new DetailTransaksi;
                $detailTransaksi->transaksi_id = $transaksi->id;
                $detailTransaksi->item_id = $item;
                $detailTransaksi->total = $request->count[$key];
                $itemUpdate = Item::find($item);
                $detailTransaksi->price = $itemUpdate->sell_price * $request->count[$key];
                if ($itemUpdate->stock - $request->count[$key] < 0) {
                    $transaksi->delete();
                    Alert::error(__('messages.error'), 'Data ' . __('models/transaksis.singular') . ' ' . __('messages.error'));
                    return redirect()->back();
                }
                $itemUpdate->stock = $itemUpdate->stock - $request->count[$key];
                $itemUpdate->save();
                $detailTransaksi->save();
            }

            Alert::success(__('messages.success'), 'Data ' . __('models/users.singular') . ' ' . __('messages.saved'));

            return redirect(route('transaksis.index'));
        } catch (\Exception $e) {
            Alert::error(__('messages.error'), 'Data ' . __('models/transaksis.singular') . ' ' . __('messages.error'));
            return redirect()->back();
        }
    }
    public function cetak($id)
    {
        $transaksi = Transaksi::findOrfail($id);
        $setting = Setting::first();
        return view('transaksis.nota', compact(['transaksi', 'setting']));
    }

    /**
     * Display the specified Transaksi.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $transaksi = $this->transaksiRepository->find($id);

        if (empty($transaksi)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/users.singular') . ' ' . __('messages.not_found'));

            return redirect(route('transaksis.index'));
        }

        return view('transaksis.show')->with('transaksi', $transaksi);
    }
}