<?php

namespace App\Http\Controllers;

use App\DataTables\CategoryDataTable;
use App\Http\Requests;
use App\Http\Requests\CreateCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;
use App\Repositories\CategoryRepository;
use Flash;
use App\Http\Controllers\AppBaseController;
use Response;
use Alert;
class CategoryController extends AppBaseController
{
    /** @var  CategoryRepository */
    private $categoryRepository;

    public function __construct(CategoryRepository $categoryRepo)
    {
        $this->categoryRepository = $categoryRepo;
    }

    /**
     * Display a listing of the Category.
     *
     * @param CategoryDataTable $categoryDataTable
     * @return Response
     */
    public function index(CategoryDataTable $categoryDataTable)
    {
        return $categoryDataTable->render('categories.index');
    }

    /**
     * Show the form for creating a new Category.
     *
     * @return Response
     */
    public function create()
    {
        return view('categories.create');
    }

    /**
     * Store a newly created Category in storage.
     *
     * @param CreateCategoryRequest $request
     *
     * @return Response
     */
    public function store(CreateCategoryRequest $request)
    {
        $input = $request->all();

        $category = $this->categoryRepository->create($input);
        Alert::success(__('messages.success'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.saved'));

        return redirect(route('categories.index'));
    }

    /**
     * Display the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function show($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.not_found'));

            return redirect(route('categories.index'));
        }

        return view('categories.show')->with('category', $category);
    }

    /**
     * Show the form for editing the specified Category.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function edit($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.not_found'));

            return redirect(route('categories.index'));
        }

        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified Category in storage.
     *
     * @param  int              $id
     * @param UpdateCategoryRequest $request
     *
     * @return Response
     */
    public function update($id, UpdateCategoryRequest $request)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.not_found'));

            return redirect(route('categories.index'));
        }

        $category = $this->categoryRepository->update($request->all(), $id);
        Alert::success(__('messages.success'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.updated'));

        return redirect(route('categories.index'));
    }

    /**
     * Remove the specified Category from storage.
     *
     * @param  int $id
     *
     * @return Response
     */
    public function destroy($id)
    {
        $category = $this->categoryRepository->find($id);

        if (empty($category)) {
            Alert::error(__('messages.error'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.not_found'));

            return redirect(route('categories.index'));
        }

        $this->categoryRepository->delete($id);

        Alert::success(__('messages.success'), 'Data ' . __('models/categories.singular') . ' ' . __('messages.deleted'));

        return redirect(route('categories.index'));
    }
}
