<?php
return [
    "not_found" => "not found",
    "saved" => "created successfully",
    "updated" => "updated successfully",
    "deleted" => "deleted successfully",
    "restore" => "restored successfully",
    "success" => "Success",
    "error" => "Error",
    "common" => [
        "hello" => "Hello",
        "login" => "Login",
        "register" => "Register",
        "logout" => "Logout",
    ]

];
