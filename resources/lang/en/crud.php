<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the crud library to build
    | the simple Crud links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'add_new' => 'Add New ',
    'action' => 'Action',
    'save' => 'Save',
    'cancel' => 'Cancel',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'show' => 'Show',
    'back'=> 'Back',
    'details' => 'Details',
    'no_entries_in_table' => 'No entries in table',

];
