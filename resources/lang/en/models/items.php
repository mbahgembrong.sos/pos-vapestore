<?php

return array(
    'singular' => 'Item',
    'plural' => 'Items',
    'fields' =>
    array(
        'id' => 'Id',
        'name' => 'Name',
        'category_id' => 'Category',
        'supplier_id' => 'Supplier',
        'stock' => 'Stock',
        'sell_price' => 'Selling Price',
        'buy_price' => 'Purchase Price',
        'image' => 'Image',
        'created_at' => 'Created At',
        'updated_at' => 'Updated At',
        'last_stock' => 'Last Stock',
        'new_stock' => 'New Stock',
        'add_stock' => 'Add Stock',
        'count_item' => 'Count Item',
    ),
);
