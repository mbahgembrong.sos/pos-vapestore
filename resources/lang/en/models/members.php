<?php

return array (
  'singular' => 'Member',
  'plural' => 'Members',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'alamat' => 'Alamat',
    'phone' => 'Phone',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
