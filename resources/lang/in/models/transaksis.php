<?php

return array(
    'singular' => 'Transaksi',
    'plural' => 'Transaksis',
    'fields' =>
    array(
        'id' => 'Id',
        'user_id' => 'User Id',
        'member_id' => 'Customer',
        'grand_total' => 'Grand Total',
        'created_at' => 'Pesan',
        'updated_at' => 'Updated At',
        'change' => 'Change',
        'payment' => 'Payment',
        'count_item' => 'Count Item',
    ),
);
