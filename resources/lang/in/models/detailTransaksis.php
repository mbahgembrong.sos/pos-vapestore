<?php

return array (
  'singular' => 'DetailTransaksi',
  'plural' => 'DetailTransaksis',
  'fields' =>
  array (
    'id' => 'Id',
    'transaksi_id' => 'Transaksi Id',
    'item_id' => 'Item',
    'total' => 'Total',
    'price' => 'Price',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
