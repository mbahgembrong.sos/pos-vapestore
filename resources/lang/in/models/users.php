<?php

return array (
  'singular' => 'User',
  'plural' => 'Users',
  'fields' =>
  array (
    'id' => 'Id',
    'role_id' => 'Role',
    'name' => 'Name',
    'email' => 'Email',
    'email_verified_at' => 'Email Verified At',
    'password' => 'Password',
    'address' => 'Address',
    'phone' => 'Phone',
    'remember_token' => 'Remember Token',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
