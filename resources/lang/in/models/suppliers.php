<?php

return array (
  'singular' => 'Supplier',
  'plural' => 'Suppliers',
  'fields' => 
  array (
    'id' => 'Id',
    'name' => 'Name',
    'sales' => 'Sales',
    'address' => 'Address',
    'phone' => 'Phone',
    'created_at' => 'Created At',
    'updated_at' => 'Updated At',
  ),
);
