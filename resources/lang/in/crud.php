<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Crud Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the crud library to build
    | the simple Crud links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |
    */

    'add_new' => 'Tambah Baru',
    'action' => 'Aksi',
    'save' => 'Simpan',
    'cancel' => 'Cancel',
    'delete' => 'Hapus',
    'edit' => 'Edit',
    'show' => 'Detail',
    'back'=> 'Kembali',
    'details' => 'Details',
    'no_entries_in_table' => 'Tidak ada entri dalam tabel',

];
