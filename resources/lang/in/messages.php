<?php
return [
    "not_found" => "tidak ditemukan",
    "saved" => "berhasil disimpan",
    "updated" => "berhasil diperbarui",
    "deleted" => "berhasil dihapus",
    "restore" => "berhasil direstore",
    "success" => "Sukses",
    "error" => "Kesalahan",
    "common" => [
        "hello" => "Hallo",
        "login" => "Login",
        "register" => "Register",
        "logout" => "Logout",
    ]

];
