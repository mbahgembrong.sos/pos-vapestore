<link href="{{ asset('assets/datatables/DataTables-1.10.25/css/dataTables.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<link href="{{ asset('assets/datatables/Buttons-1.7.1/css/buttons.bootstrap4.min.css') }}" rel="stylesheet" type="text/css" />
<style>
    table.dataTable {
        clear: both;
        margin-top: 0px !important;
        margin-bottom: 0px !important;
        max-width: none !important;
        border-collapse: separate !important;
        border-spacing: 0;
    }
</style>
