<li class="side-menus {{ Request::is('dasboard*') ? 'active' : '' }}">
    <a class="nav-link" href="/home">
        <i class=" fas fa-building"></i><span>Dashboard</span>
    </a>
</li>

<li class="{{ Request::is('transaksis*') ? 'active' : '' }}">
    <a href="{{ route('transaksis.index') }}"><i class="fas fa-cash-register"></i><span>@lang('models/transaksis.plural')</span></a>
</li>
@if (auth()->user()->role->name == 'Pemilik')
    <li class="{{ Request::is('users*') ? 'active' : '' }}">
        <a href="{{ route('users.index') }}"><i class="fas fa-user"></i><span>@lang('models/users.plural')</span></a>
    </li>
    <li class="{{ Request::is('roles*') ? 'active' : '' }}">
        <a href="{{ route('roles.index') }}"><i class="fas fa-user-tag"></i><span>@lang('models/roles.plural')</span></a>
    </li>
@endif
<li class="{{ Request::is('members*') ? 'active' : '' }}">
    <a href="{{ route('members.index') }}"><i class="fas fa-user-check"></i><span>@lang('models/members.plural')</span></a>
</li>

@if (auth()->user()->role->name == 'Pemilik')
    <li class="{{ Request::is('categories*') ? 'active' : '' }}">
        <a href="{{ route('categories.index') }}"><i class="fas fa-sitemap"></i><span>@lang('models/categories.plural')</span></a>
    </li>

    <li class="{{ Request::is('items*') ? 'active' : '' }}">
        <a href="{{ route('items.index') }}"><i class="fas fa-box"></i><span>@lang('models/items.plural')</span></a>
    </li>
    <li class="{{ Request::is('suppliers*') ? 'active' : '' }}">
        <a href="{{ route('suppliers.index') }}"><i class="fas fa-people-carry"></i><span>@lang('models/suppliers.plural')</span></a>
    </li>
    <li class="{{ Request::is('laporan*') ? 'active' : '' }}">
        <a href="{{ route('laporan.index') }}"><i class="fas fa-sticky-note"></i><span>laporan</span></a>
    </li>
    <li class="{{ Request::is('setting*') ? 'active' : '' }}">
        <a href="{{ route('setting') }}"><i class="fas fa-cog"></i><span>setting</span></a>
    </li>
@endif
