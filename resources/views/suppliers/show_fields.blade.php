<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/suppliers.fields.name').':') !!}
    <p>{{ $supplier->name }}</p>
</div>

<!-- Sales Field -->
<div class="form-group">
    {!! Form::label('sales', __('models/suppliers.fields.sales').':') !!}
    <p>{{ $supplier->sales }}</p>
</div>

<!-- Address Field -->
<div class="form-group">
    {!! Form::label('address', __('models/suppliers.fields.address').':') !!}
    <p>{{ $supplier->address }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', __('models/suppliers.fields.phone').':') !!}
    <p>{{ $supplier->phone }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/suppliers.fields.created_at').':') !!}
    <p>{{ $supplier->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/suppliers.fields.updated_at').':') !!}
    <p>{{ $supplier->updated_at }}</p>
</div>

