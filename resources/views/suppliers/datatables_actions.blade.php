{!! Form::open(['route' => ['suppliers.destroy', $id], 'method' => 'delete']) !!}
<div class='btn-group'>
    <a href="{{ route('suppliers.show', $id) }}" class='btn btn-primary btn-xs' data-toggle="tooltip" title="Show">
        <i class="far fa-eye"></i>
    </a>
    <a href="{{ route('suppliers.edit', $id) }}" class='btn btn-warning btn-xs' data-toggle="tooltip" title="Edit">
        <i class="fas fa-edit"></i>
    </a>
    {!! Form::button('<i class="fas fa-trash"></i>', [
        'type' => 'reset',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete',
        'data-toggle' => 'tooltip',
    ]) !!}
</div>
{!! Form::close() !!}
<script>
    $('.btn-danger').click(function() {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var form = $(this).closest("form");
                this.form.submit();
            } else {
                false;
            }
        })
    });
</script>
