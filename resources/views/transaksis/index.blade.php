@extends('layouts.app')
@section('title')
    @lang('models/transaksis.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/transaksis.plural')</h1>
            @if (auth()->user()->role->name == 'Karyawan')
                <div class="section-header-breadcrumb">
                    <a href="{{ route('transaksis.create') }}" class="btn btn-primary form-btn">@lang('crud.add_new')<i
                            class="fas fa-plus"></i></a>
                </div>
            @endif
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('transaksis.table')
                </div>
            </div>
        </div>

    </section>
@endsection
@push('scripts')
    <script>
        window.location
    </script>
@endpush
