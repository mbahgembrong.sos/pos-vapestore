<!-- Member Id Field -->
<div class="form-group col-sm-12">
    <div class="row">
        <div class="col-md-4">
            {!! Form::label('member_id', __('models/transaksis.fields.member_id') . ':') !!}
        </div>
        <div class="col-md-4">
            <input type="radio" name="member" id="radioMember"value="Member" data-toggle="collapse" data-target="#member"
                aria-expanded="false" aria-controls="member"> Member
        </div>
        <div class="col-md-4">
            <input type="radio" name="member" id="radioNon" value="Non Member" data-toggle="collapse"
                data-target="#nonMember" aria-expanded="false" aria-controls="nonMember"> Non Member
        </div>
    </div>
    {{-- member --}}
    {!! Form::select('member_id', $members, null, ['class' => 'form-control collapse', 'id' => 'member']) !!}
</div>

{{-- non member --}}
<div class="form-group col-sm-6" id="form_name">
    {!! Form::label('name', 'Nama Pelanggan :') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6" id="form_telp">
    {!! Form::label('telp', 'Telp Pelanggan :') !!}
    {!! Form::text('telp', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12" id="form_address">
    {!! Form::label('address', 'Alamat Pelanggan :') !!}
    {!! Form::textarea('address', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-12" id="transactions">
    {!! Form::label('item', __('models/detailTransaksis.fields.item_id') . ':') !!}
    <div class="form-group fieldGroup" data-id="1">
        <div class="input-group">
            <select name="items[]" class="form-control">
                <option value="" disabled selected>Pilih Items</option>
                @foreach ($items as $item)
                    <option value="{{ $item->id }}" data-price="{{ $item->sell_price }}"
                        data-stock="{{ $item->stock }}" data-discount="{{ $item->category->discount }}">
                        {{ $item->name }}</option>
                @endforeach
            </select>
            {!! Form::number('count[]', null, ['class' => 'form-control counts', 'min' => 0, 'placeholder' => '']) !!}
            {!! Form::number('total[]', null, [
                'class' => 'form-control total',
                'min' => 0,
                'placeholder' => '',
                'readonly',
            ]) !!}
            <div class="input-group-addon ml-3">
                <a href="javascript:void(0)" class="btn btn-success addMore"><i class="fas fa-plus"></i></a>
            </div>
        </div>
    </div>
</div>
{{-- trash --}}
<div class="form-group fieldGroupCopy" style="display: none;">
    <div class="input-group">
        <select name="items[]" class="form-control">
            <option value="" disabled selected>Pilih Items</option>
            @foreach ($items as $item)
                <option value="{{ $item->id }}" data-price="{{ $item->sell_price }}"
                    data-stock="{{ $item->stock }}" data-discount="{{ $item->category->discount }}">
                    {{ $item->name }}</option>
            @endforeach
        </select>
        {!! Form::number('count[]', null, ['class' => 'form-control counts', 'min' => 0, 'placeholder' => '']) !!}
        {!! Form::number('total[]', null, [
            'class' => 'form-control total',
            'min' => 0,
            'placeholder' => '',
            'readonly',
        ]) !!}
        <div class="input-group-addon ml-3">
            <a href="javascript:void(0)" class="btn btn-danger remove"><i class="fas fa-trash"></i></a>
        </div>
    </div>
</div>
<div class="col-12">
    <div class="row">
        <div class="form-group col">
            {!! Form::label('grand_total', __('models/transaksis.fields.grand_total') . ':') !!}
            {!! Form::number('grand_total', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
        <div class="form-group col" id="discountLayout"style="display: none;">
            {!! Form::label('discount', __('models/categories.fields.discount') . ':') !!}
            {!! Form::number('discount', null, ['class' => 'form-control', 'readonly']) !!}
        </div>
    </div>
</div>
<div class="form-group col-sm-6">
    {!! Form::label('payment', __('models/transaksis.fields.payment') . ':') !!}
    {!! Form::number('payment', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('change', __('models/transaksis.fields.change') . ':') !!}
    {!! Form::number('change', null, ['class' => 'form-control', 'readonly', 'min' => 0]) !!}
</div>
<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary', 'disabled', 'id' => 'buttonSubmit']) !!}
    <a href="{{ route('transaksis.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
@push('scripts')
    <script>
        $(document).ready(function() {
            $('#radioNon').attr('checked', true);
            $('#radioNon').click(function() {
                $('.collapse').collapse('hide')
                $('#member').val('');
                $('#form_name').show();
                $('#form_address').show();
                $('#form_telp').show();

                calculate()
            });
            $('#radioMember').click(function(e) {
                $('#form_name').hide();
                $('#form_address').hide();
                $('#form_telp').hide();
                $('#form_name').val('');
                $('#form_address').val('');
                $('#form_telp').val('');
                calculate()
            });
            $('#radioMember').on('shown.bs.collapse', function() {
                calculate()
                $('#radioMember').click(function(e) {
                    $('#member').colapse('show');
                    calculate()
                });
            });
            $('#transactions').on('click', '.fieldGroup', function() {
                var fieldGroup = $(this);
                $(this).find('select').on('change', function(e) {
                    calculate()
                });
                $(this).find('input').on('keyup', function(e) {
                    if (e.target.value > $(this).data('stock')) {
                        e.target.value = $(this).data('stock');
                    }
                    calculate()
                });
            });
            $('input[name="payment"]').on('keyup', function(e) {
                calculate()
            });
        });


        function calculate() {
            var grandTotal = 0;
            var discountTotal = 0;
            var payment = $('#payment').val();
            var fieldGroup = $('body').find('.fieldGroup');
            fieldGroup.each(function() {
                var price = 0;
                var total = 0;
                var discount = 0;
                count = $(this).find('.counts').val();
                price = $(this).find('select').find(':selected').data('price');
                discount = $(this).find('select').find(':selected').data('discount');
                total = count * price;
                $(this).find('.total').val(total);
                if ($('#radioMember').is(':checked')) {
                    discountTotal += (total * discount / 100);
                    total = total - (total * discount / 100);
                    $('#discountLayout').show();
                } else {
                    discount = 0;
                }
                grandTotal += total;
                change = payment - grandTotal;
                if (change < 0 && grandTotal == 0) {
                    change = change;
                    $('#buttonSubmit').attr('disabled', true);
                } else {
                    $('#buttonSubmit').attr('disabled', false);
                }

            });
            $('#grand_total').val(grandTotal);
            $('#discount').val(discountTotal);
            $('#change').val(change);
        }
        // input multiple field
        $(document).ready(function() {
            // membatasi jumlah inputan
            var maxGroup = 10;

            //melakukan proses multiple input
            $(".addMore").click(function() {
                var layanansInput = $('body').find('.fieldGroup').length;
                if (layanansInput < maxGroup) {
                    var fieldHTML = '<div class="form-group fieldGroup" data-id="' + (layanansInput + 1) +
                        '">' + $(".fieldGroupCopy").html() + '</div>';
                    $('body').find('.fieldGroup:last').after(fieldHTML);
                } else {
                    alert('Maksimal ' + maxGroup + ' Layanan.');
                }
            });
            //remove fields group
            $("body").on("click", ".remove", function() {
                $(this).parents(".fieldGroup").remove();
            });
        });
    </script>
@endpush
