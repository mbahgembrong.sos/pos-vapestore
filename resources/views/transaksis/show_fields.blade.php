<!-- User Id Field -->
<div class="row">
    <div class="form-group col-sm 6">
        {!! Form::label('user_id', 'Karyawan ' . ':') !!}
        <p>{{ $transaksi->user->name }}</p>
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('created_at', __('models/transaksis.fields.created_at') . ':') !!}
        <p>{{ $transaksi->created_at }}</p>
    </div>
    <!-- Member Id Field -->

</div>
<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('member_id', __('models/transaksis.fields.member_id') . ':') !!}
        <p>{{ $transaksi->name ?? $transaksi->member->name ?: 'non member' }}</p>
    </div>
    <div class="form-group col-sm-6">
        {!! Form::label('member_id', 'Telp ' . ':') !!}
        <p>{{ $transaksi->telp ?? $transaksi->member->phone }}</p>
    </div>
</div>
<div class="row">
    <div class="form-group col-sm-12">
        {!! Form::label('member_id', 'Address ' . ':') !!}
        <p>{{ $transaksi->address ?? $transaksi->member->alamat }}</p>
    </div>
</div>

<div class="row">
    <div class="form-group col-sm-6">
        {!! Form::label('count_item', __('models/transaksis.fields.count_item') . ':') !!}
        <p>{{ $transaksi->detailTransaksi->count() }} items</p>
    </div>

    <div class="form-group col-6">
        {!! Form::label('grand_total', __('models/transaksis.fields.grand_total') . ':') !!}
        <p>Rp. {{ $transaksi->grand_total }}</p>
    </div>

</div>
<div class="form-group col-sm-12" id="transactions">
    {!! Form::label('item', __('models/detailTransaksis.fields.item_id') . ':') !!}
    <div class="form-group fieldGroup" data-id="1">
        @foreach ($transaksi->detailTransaksi as $item)
            <div class="input-group">
                {!! Form::text('count[]', $item->item->name, [
                    'class' => 'form-control counts',
                    'min' => 0,
                    'readonly',
                ]) !!}
                {!! Form::text('count[]', $item->total, [
                    'class' => 'form-control counts',
                    'min' => 0,
                    'readonly',
                ]) !!}
                {!! Form::text('total[]', $item->price, [
                    'class' => 'form-control total',
                    'min' => 0,
                    'placeholder' => '',
                    'readonly',
                ]) !!}

            </div>
    </div>
    @endforeach
</div>
