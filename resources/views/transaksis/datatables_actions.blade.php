 <a href="{{ route('transaksis.show', $id) }}" class='btn btn-primary btn-xs' data-toggle="tooltip" title="show">
     <i class="fas fa-eye"></i>
 </a>

 @if (auth()->user()->role->name != 'Pemilik')
     <a href="{{ route('transaksis.cetak', $id) }}" class='btn btn-success btn-xs' target="blank" data-toggle="tooltip"
         title="struk">
         <i class="fas fa-copy"></i>
     </a>
 @endif
