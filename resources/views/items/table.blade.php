@section('css')
    @include('layouts.datatables_css')
@endsection
{!! $dataTable->table(['width' => '100%', 'class' => 'table table-striped table-bordered']) !!}

@push('scripts')
    @include('layouts.datatables_js')
    {!! $dataTable->scripts() !!}
    <script type="text/javascript">
        // modalAddStock
        $('#modalAddStock').on('show.bs.modal', function(event) {
            var button = $(event.relatedTarget)
            console.log(button.data())
            var name = button.data('name');
            var last_stock = button.data('stock');
            var id = button.data('id');
            var modal = $(this)
            modal.find('.modal-body #id').val(id);
            modal.find('.modal-body #name').val(name);
            modal.find('.modal-body #last_stock').val(last_stock);
             modal.find('.modal-footer #submit').click(() => {
                // modal.find('.modal-body #buy_price').val('');
                // modal.find('.modal-body #new_stock').val('');
                // location.href = "transaksi/bayar/" + idTransaksi;
            })
        });
    </script>
@endpush
