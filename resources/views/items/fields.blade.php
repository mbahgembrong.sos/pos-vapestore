<!-- Category Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('name', __('models/items.fields.name') . ':') !!}
    {!! Form::text('name', null, ['class' => 'form-control']) !!}
</div>
<div class="form-group col-sm-6">
    {!! Form::label('category_id', __('models/items.fields.category_id') . ':') !!}
    {!! Form::select('category_id', $categorys, null, ['class' => 'form-control']) !!}
</div>

<!-- Supplier Id Field -->
<div class="form-group col-sm-6">
    {!! Form::label('supplier_id', __('models/items.fields.supplier_id') . ':') !!}
    {!! Form::select('supplier_id', $suppliers, null, ['class' => 'form-control']) !!}
</div>


<!-- Price Field -->
<div class="form-group col-sm-6">
    {!! Form::label('sell_price', __('models/items.fields.sell_price') . ':') !!}
    {!! Form::number('sell_price', null, ['class' => 'form-control']) !!}
</div>

<!-- Image Field -->
<div class="form-group col-sm-6">
    {!! Form::label('image', __('models/items.fields.image') . ':') !!}
    {!! Form::file('image') !!}
</div>
<div class="clearfix"></div>

<!-- Submit Field -->
<div class="form-group col-sm-12">
    {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
    <a href="{{ route('items.index') }}" class="btn btn-light">@lang('crud.cancel')</a>
</div>
@push('scripts')
    <script>
        // filepond
        // image previews, cropping, resizing, etc.
        FilePond.registerPlugin(
            FilePondPluginFileValidateType,
            FilePondPluginImagePreview,
        );
        FilePond.create(
            document.querySelector('input[name="image"]'), {
                labelIdle: `Drag & Drop your picture or <span class="filepond--label-action">Browse</span>`,
                storeAsFile: true,
                imagePreviewMaxHeight: 150,
                imagePreviewTransparencyIndicator: 'grid',
                acceptedFileTypes: ['image/*'],
                fileValidateTypeDetectType: (source, type) => new Promise((resolve, reject) => {
                    resolve(type);
                }),
            }
        );
    </script>
@endpush
