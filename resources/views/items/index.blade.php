@extends('layouts.app')
@section('title')
    @lang('models/items.plural')
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>@lang('models/items.plural')</h1>
            <div class="section-header-breadcrumb">
                <a href="{{ route('items.create') }}" class="btn btn-primary form-btn">@lang('crud.add_new')<i
                        class="fas fa-plus"></i></a>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    @include('items.table')
                </div>
            </div>
        </div>

    </section>
    {{-- modalAddstock --}}
    <div class="modal fade" id="modalAddStock" tabindex="-1" role="dialog" aria-labelledby="modalAddStockTitle"
        aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered" role="document">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="modalAddStockTitle">@lang('models/items.fields.add_stock')</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form action="{{ route('items.stock') }}" method="post">
                    <div class="modal-body">
                        @csrf
                        <input type="hidden" name="id" id="id">
                        <div class="form-group">
                            <label for="name" class="col-form-label">@lang('models/items.fields.name')</label>
                            <input type="text" class="form-control" disabled id="name">
                        </div>
                        <div class="form-group">
                            <label for="last_stock" class="col-form-label">@lang('models/items.fields.last_stock')</label>
                            <input class="form-control" disabled id="last_stock"></input>
                        </div>
                        <div class="form-group">
                            <label for="new_stock" class="col-form-label">@lang('models/items.fields.new_stock')</label>
                            <input class="form-control" id="new_stock" name="new_stock" type="number" min="0"></input>
                        </div>
                        <div class="form-group">
                            <label for="buy_price" class="col-form-label">@lang('models/items.fields.buy_price')</label>
                            <input class="form-control" type="number" min="0" name="buy_price" id="buy_price"></input>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button type="button" class="btn btn-light" data-dismiss="modal">@lang('crud.cancel')</button>
                        <button type="submit" id="submit" class="btn btn-primary">@lang('crud.save')</button>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection
