{!! Form::open(['route' => ['items.destroy', $id], 'method' => 'delete', 'id' => 'form-delete']) !!}
<div class='btn-group'>
    {{-- <a href="{{ route('items.show', $id) }}" class='btn btn-primary btn-xs'data-toggle="tooltip" title="Show">
        <i class="far fa-eye"></i>
    </a> --}}
    <a data-id="{{ $id }}" data-name="{{ $name }}" data-stock="{{$stock}}" data-toggle="modal"
        data-target="#modalAddStock" data-stock="{{ $stock }}" class='btn btn-success btn-xs text-white'
         title="Add Stock">
        <i class="fas fa-folder-plus"></i>
    </a>
    <a href="{{ route('items.edit', $id) }}" class='btn btn-warning btn-xs' data-toggle="tooltip" title="Edit">
        <i class="fas fa-edit"></i>
    </a>
    {!! Form::button('<i class="fas fa-trash"></i>', [
        'type' => 'reset',
        'class' => 'btn btn-danger btn-xs',
        'title' => 'Delete',
        'data-toggle' => 'tooltip',
    ]) !!}
</div>
{!! Form::close() !!}

<script type="text/javascript">
    $('.btn-danger').click((e) => {
        return Swal.fire({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            icon: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!'
        }).then((result) => {
            if (result.value) {
                var form = $('#form-delete');
                console.log(form);
                form.submit();
            } else {
                false;
            }
        })
    });
</script>
