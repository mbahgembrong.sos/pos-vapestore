@extends('layouts.app')
@section('title')
    laporan
@endsection
@push('css')
    @include('layouts.datatables_css')
@endpush
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>laporan</h1>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Items Transaction</h4>
                                </div>
                                <div class="card-body">
                                    <canvas id="weekChart" height="80"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Tabel Laporan</h4>
                                </div>
                                <div class="card-body">
                                    <table class="table table-striped table-hover" id="table">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Date</th>
                                                <th>Item Terjual</th>
                                                <th>Grand Total</th>
                                                <th>Pendapatan</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @php
                                                $no = 1;
                                            @endphp
                                            @forelse ($laporan as $item)
                                                {{-- @if ($item['items']->sum('total') != 0) --}}
                                                @php
                                                    $items = '';
                                                    if ($item['items']->sum('total') != 0) {
                                                        foreach ($item['items'] as $key => $value) {
                                                            $items .= $value['name'] . ' (' . $value['total'] . ')' . ', ';
                                                        }
                                                    } else {
                                                        $items = '-';
                                                    }
                                                @endphp
                                                <tr>
                                                    <td>{{ $no++ }}</td>
                                                    <td>{{ $item['date'] }}</td>
                                                    <td>{{ $items }}</td>
                                                    <td>Rp. {{ $item['transaksi'] }}</td>
                                                    <td>Rp. {{ $item['pendapatan'] }}</td>
                                                </tr>
                                                {{-- @endif --}}
                                            @empty
                                                <tr>
                                                    <td colspan="6">@lang('crud.no_entries_in_table')</td>
                                                </tr>
                                            @endforelse
                                        </tbody>
                                        <tfoot>
                                            <tr>
                                                <td>#</td>
                                                <td colspan="2" class="text-center">Total</td>
                                                <td>Rp. {{ $laporan->sum('transaksi') }}</td>
                                                <td>Rp. {{ $laporan->sum('pendapatan') }}</td>
                                            </tr>
                                        </tfoot>
                                    </table>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@push('scripts')
    @include('layouts.datatables_js')
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/dataTables.buttons.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.flash.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jszip/3.1.3/jszip.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/pdfmake.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.1.53/vfs_fonts.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.html5.min.js"></script>
    <script src="https://cdn.datatables.net/buttons/1.5.6/js/buttons.print.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#table').DataTable({
                searching: false,
                buttons: [
                    'copy', 'csv', 'excel', 'pdf', 'print'
                ],
                dom: 'Bfrtip',
            });
        });
        var laporan = {!! json_encode($laporan) !!}
        var backgroundColors = []
        var laporanPendapatan = [];
        var date = [];

        function random_rgba() {
            var o = Math.round,
                r = Math.random,
                s = 255;
            return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(3) + ')';
        }
        laporan.forEach(function(item) {
            laporanPendapatan.push(item.pendapatan);
            date.push(item.date);
        })
        console.log(laporanPendapatan)


        // var weekChart = document.getElementById("weekChart").getContext('2d');

        var myChart = new Chart(weekChart, {
            type: 'line',
            data: {
                labels: date,
                datasets: [{
                    label: 'Pendapatan',
                    data: laporanPendapatan,
                    backgroundColor: 'rgba(255, 159, 64, 0.2)',
                    borderColor: 'rgba(255, 159, 64, 1)',
                    borderWidth: 1,
                    lineTension: 0.4,
                }]
            },

        });
    </script>
@endpush
