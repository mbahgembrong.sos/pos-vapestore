<!-- Name Field -->
<div class="form-group">
    {!! Form::label('name', __('models/members.fields.name').':') !!}
    <p>{{ $member->name }}</p>
</div>

<!-- Alamat Field -->
<div class="form-group">
    {!! Form::label('alamat', __('models/members.fields.alamat').':') !!}
    <p>{{ $member->alamat }}</p>
</div>

<!-- Phone Field -->
<div class="form-group">
    {!! Form::label('phone', __('models/members.fields.phone').':') !!}
    <p>{{ $member->phone }}</p>
</div>

<!-- Created At Field -->
<div class="form-group">
    {!! Form::label('created_at', __('models/members.fields.created_at').':') !!}
    <p>{{ $member->created_at }}</p>
</div>

<!-- Updated At Field -->
<div class="form-group">
    {!! Form::label('updated_at', __('models/members.fields.updated_at').':') !!}
    <p>{{ $member->updated_at }}</p>
</div>

