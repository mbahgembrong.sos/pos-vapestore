@extends('layouts.app')
@section('title')
    Setting Toko
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h3 class="page__heading m-0">Setting Toko</h3>
            <div class="filter-container section-header-breadcrumb row justify-content-md-end">
                {{-- <a href="{{ route('') }}" class="btn btn-primary">@lang('crud.back')</a> --}}
            </div>
        </div>
        <div class="content">
            @include('stisla-templates::common.errors')
            <div class="section-body">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="card">
                            <div class="card-body ">
                                {!! Form::model($setting, ['route' => ['setting.update'], 'method' => 'post','files' => true]) !!}
                                @csrf
                                <div class="row">
                                    <!-- Name Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('name', __('models/members.fields.name') . ':') !!}
                                        {!! Form::text('name', null, ['class' => 'form-control']) !!}
                                    </div>
                                    <!-- Phone Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('phone', __('models/members.fields.phone') . ':') !!}
                                        {!! Form::text('phone', null, ['class' => 'form-control']) !!}
                                    </div>

                                    <!-- Alamat Field -->
                                    <div class="form-group col-sm-12 col-lg-12">
                                        {!! Form::label('alamat', __('models/members.fields.alamat') . ':') !!}
                                        {!! Form::textarea('alamat', null, ['class' => 'form-control']) !!}
                                    </div>


                                    <!-- Phone Field -->
                                    <div class="form-group col-sm-6">
                                        {!! Form::label('logo', 'Logo  ' . ':') !!}
                                        {!! Form::file('logo') !!}
                                    </div>

                                    <!-- Submit Field -->
                                    <div class="form-group col-sm-12">
                                        {!! Form::submit(__('crud.save'), ['class' => 'btn btn-primary']) !!}
                                        {{-- <a href="{{ route('/') }}" class="btn btn-light">@lang('crud.cancel')</a> --}}
                                    </div>

                                </div>

                                {!! Form::close() !!}
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
