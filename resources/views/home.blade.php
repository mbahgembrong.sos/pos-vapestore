@extends('layouts.app')
@section('title')
    Home
@endsection
@section('content')
    <section class="section">
        <div class="section-header">
            <h1>Home</h1>
        </div>
        <div class="row">
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-success">
                        <i class="fas fa-money-bill"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Income in day</h4>
                        </div>
                        <div class="card-body">
                            Rp. {{ $pendapatanDays }}
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-danger">
                        <i class="fas fa-cash-register"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Count transaction in days</h4>
                        </div>
                        <div class="card-body">
                            {{ $countTransaksiDays }} Transaction
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-4 col-md-6 col-sm-6 col-12">
                <div class="card card-statistic-1">
                    <div class="card-icon bg-primary">
                        <i class="fas fa-box"></i>
                    </div>
                    <div class="card-wrap">
                        <div class="card-header">
                            <h4>Count item</h4>
                        </div>
                        <div class="card-body">
                            {{ $countItem }} Items
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="section-body">
            <div class="card">
                <div class="card-body">
                    <div class="row">
                        <div class="col-lg-12 col-md-12 col-12 col-sm-12">
                            <div class="card">
                                <div class="card-header">
                                    <h4>Items Transaction</h4>
                                    <div class="card-header-action">
                                        <div class="btn-group">
                                            <a href="#weekChart" class="btn btn-primary" data-toggle="collapse"
                                                aria-expanded="false" aria-controls="weekChart">Week</a>
                                        </div>
                                    </div>
                                </div>
                                <div class="card-body">
                                    <canvas id="weekChart" height="80"></canvas>
                                    <canvas id="monthChart" height="80"></canvas>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </section>
@endsection
@push('scripts')
    <script>
        var itemPopulerDay = {!! $itemPopulerDay !!};
        var days = {!! json_encode($days) !!};
        var backgroundColors = []

        function random_rgba() {
            var o = Math.round,
                r = Math.random,
                s = 255;
            return 'rgba(' + o(r() * s) + ',' + o(r() * s) + ',' + o(r() * s) + ',' + r().toFixed(3) + ')';
        }
        itemPopulerDay.forEach(function(item) {
            item.backgroundColor = random_rgba();
            item.borderColor =  item.backgroundColor;
        })

        var weekChart = document.getElementById("weekChart").getContext('2d');

        var myChart = new Chart(weekChart, {
            type: 'line',
            data: {
                labels: days,
                datasets: itemPopulerDay
            },

        });
    </script>
@endpush
