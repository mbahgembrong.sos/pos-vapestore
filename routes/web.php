<?php

use App\Http\Controllers\TestController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return redirect('/home');
});

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Auth::routes();
Route::middleware('auth')->group(function () {
    Route::middleware('auth.pemilik')->group(
        function () {
            Route::resource('users', App\Http\Controllers\UserController::class);
            Route::prefix('users')->group(function () {
                Route::post('/change-profile/{id}', [App\Http\Controllers\UserController::class, 'changeProfile'])->name('users.change-profile');
                Route::post('/change-password/{id}', [App\Http\Controllers\UserController::class, 'changePassword'])->name('users.change-password');
            });
            Route::resource('roles', App\Http\Controllers\RoleController::class);
            Route::resource('suppliers', App\Http\Controllers\SupplierController::class);
            Route::prefix('laporan')->group((function () {
                Route::get('/', [App\Http\Controllers\LaporanController::class, 'index'])->name('laporan.index');
            }));
            Route::prefix('setting')->group(function () {
                Route::get('/', [App\Http\Controllers\SettingController::class, 'edit'])->name('setting');
                Route::post('/', [App\Http\Controllers\SettingController::class, 'update'])->name('setting.update');
            });
        }
    );

    Route::resource('members', App\Http\Controllers\MemberController::class);


    Route::resource('categories', App\Http\Controllers\CategoryController::class);


    Route::resource('items', App\Http\Controllers\ItemController::class, );
    Route::prefix('items')->group(
        function () {
            Route::post('stock', [App\Http\Controllers\ItemController::class, 'stock'])->name('items.stock');
            Route::get('json/show/{id}', [App\Http\Controllers\ItemController::class, 'jsonShow'])->name('items.json.show');
        }
    );


    Route::resource('transaksis', App\Http\Controllers\TransaksiController::class);
    Route::prefix('transaksis')->group(function () {
        Route::get('/cetak/{id}', [App\Http\Controllers\TransaksiController::class, 'cetak'])->name('transaksis.cetak');
    });

    Route::resource('detailTransaksis', App\Http\Controllers\DetailTransaksiController::class);
    Route::get('/test', [TestController::class, 'index']);
});