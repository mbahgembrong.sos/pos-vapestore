# Laravel PHP Framework
This App using Laravel version 5.3

## Official Laravel Documentation

Documentation for the framework can be found on the [Laravel website](http://laravel.com/docs).

# To use this app

- Clone project from this repository

- Run `composer install`
- Run `npm install `
- copy `.env.example` to `.env`
- config your database in ` .env ` file

- Run `php artisan key:generate`

- Run `php artisan migrate --seed`

- Run `php artisan storage:link`

- Run `php artisan serve`

- Done.
  
  ### Account
  - Pemilik 
    - email `pemilik@gmail.com`
    - pass `pemilik`
  - Karyawan
    - email `karyawan@gmail.com`
    - pass `karyawan`
